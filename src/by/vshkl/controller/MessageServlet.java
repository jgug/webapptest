package by.vshkl.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.vshkl.model.Message;
import by.vshkl.model.Person;

public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Person person = (Person) request.getSession().getAttribute("person");
		String email = request.getParameter("email");
		String message = request.getParameter("message");
		
		Message messageObj = new Message(person, email, message);
		
		request.setAttribute("message", messageObj);
		RequestDispatcher dispatcher = request.getRequestDispatcher("summary.jsp");
		dispatcher.forward(request, response);
	}

}