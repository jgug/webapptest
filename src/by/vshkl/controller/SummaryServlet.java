package by.vshkl.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.bcel.internal.generic.GOTO;

import by.vshkl.model.Message;

public class SummaryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Message message = (Message) request.getSession().getAttribute("message");
		
		Connection connection = null;
		Statement statement;
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO messages_all (fname, lname, email, age, message) VALUES(")
			.append("'").append(message.getPerson().getFirstname()).append("', ")
			.append("'").append(message.getPerson().getLastname()).append("', ")
			.append("'").append(message.getEmail()).append("', ")
			.append(message.getPerson().getAge()).append(", ")
			.append("'").append(message.getMessage()).append("'")
			.append(");");
		
		System.out.println(sb.toString());
		
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection(
					"jdbc:sqlite:"+getServletContext().getResource("/WEB-INF/database.db"));
			
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			connection.setAutoCommit(false);
			statement.executeUpdate(sb.toString());
			connection.commit();
			connection.setAutoCommit(true);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		response.sendRedirect("start.jsp");
	}
}
