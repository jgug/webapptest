package by.vshkl.model;

public class Message {
	private Person person;
	private String email;
	private String message;
	
	public Message() {
		
	}
	
	public Message(Person person, String email, String message) {
		this.person = person;
		this.email = email;
		this.message = message;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
