package by.vshkl.model;

public class Person {
	private String firstname;
	private String lastname;
	private String sex;
	private int age;
	
	public Person() {
	}
	
	public Person(String firstname, String lastname, String sex, int age) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.sex = sex;
		this.age = age;
	}

	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
