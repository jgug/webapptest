# README #

Test web app.

This app can store and show "visitors" messages. For storing messages used SQLite database.

### Pages ###

* Start page with list of messages
* Visitor information input
* Visitor message and contact e-mail
* Summary before submitting

### Note ###

* App use this [SQLite JDBC Driver](https://jgug@bitbucket.org/xerial/sqlite-jdbc)