<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="StartStyles.css">
    <title>Messages</title>
</head>
<body>
    <h1 align="center">Cool message system</h1>
    <%
    Class.forName("org.sqlite.JDBC");
    Connection connection = null;
    ResultSet rs;
    try {
        connection = DriverManager.getConnection(
        		"jdbc:sqlite:"+getServletContext().getResource("/WEB-INF/database.db"));
        
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);
        
        rs = statement.executeQuery("SELECT date, fname, message FROM messages_all;");
    %>
    <table>
        <tr>
            <th width="15%">Date</th>
            <th width="15%">Name</th>
            <th width="75%">Message</th>
        </tr>
        <% while (rs.next()) { %>
        <tr>
            <td><%= rs.getString(1) %></td>
            <td><%= rs.getString(2) %></td>
            <td><%= rs.getString(3) %></td>
        </tr>
        <% } %>
    </table><br>
    <form method="get" action="Start">
        <div align="center"><input type="submit" name="add" value="Add new message"/></div>
    </form>
    <%
    } catch(SQLException e) {
        e.printStackTrace();
    } finally {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
    %>
</body>
</html>