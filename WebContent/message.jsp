<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="by.vshkl.model.Person"%>
<% 
    Person person = (Person) request.getAttribute("person");
    session.setAttribute("person", person);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="MessageStyle.css">
    <title>Message input</title>
</head>
<body>
    <h1 align="center">Message page</h1>
    <!-- Personal data -->
    <fieldset>
	    <legend><b>Your personal data</b></legend>
	        <% if (person != null) { %>
	        <label>Firstname: <%= person.getFirstname() %></label><br>
	        <label>Lastname: <%= person.getLastname() %></label><br>
	        <label>Sex: <%= person.getSex() %></label><br>
	        <label>Age: <%= person.getAge() %></label><br>
	        <% } %>
    </fieldset><br>
    <!-- Message -->
    <form method="post" action="Message">
        <fieldset>
        <legend><b>Message & contact info</b></legend>
            <label>Email:</label>
            <input type="email" class="inputText" name="email" title="Input your email to contact" required="required"><br>
            <label>Message:</label><br>
            <textarea rows="4" cols="20" name="message" maxlength="255' title="Input your message" required="required"></textarea>
        </fieldset>
        <!-- Submit -->
        <p align="center">
            <input type="reset" class="button" name="reset" value="Reset"/>
            <input type="submit" class="button" name="submit" value="Submit"/>
        </p>
    </form>
</body>
</html>