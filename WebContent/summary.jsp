<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="by.vshkl.model.Message"%>
<%
    Message message = (Message) request.getAttribute("message");
    session.setAttribute("message", message);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="SummaryStyle.css">
    <title>Summary</title>
</head>
<body>
    <h1 align="center">Summary page</h1>
    <!-- Full message data -->
    <form method="post" action="Summary">
        <fieldset>
	        <legend><b>Summary message data</b></legend>
	            <% if (message != null) { %>
	            <label>Firstname: <%= message.getPerson().getFirstname() %></label><br>
	            <label>Lastname: <%= message.getPerson().getLastname() %></label><br>
	            <label>Sex: <%= message.getPerson().getSex() %></label><br>
	            <label>Age: <%= message.getPerson().getAge() %></label><br><br>
	            <label>Email: <%= message.getEmail() %></label><br><br>
	            <label>Message: <br><%= message.getMessage() %></label><br>
	            <% } %>
        </fieldset><br>
        <p align="center">
            <input type="submit" class="button" name="submit" value="Submit">
        </p>
    </form> 
</body>
</html>